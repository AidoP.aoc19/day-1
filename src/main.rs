use std::{fs::File, io::{BufRead, BufReader}};

#[cfg(not(feature="part2"))]
fn main() -> std::io::Result<()> {
    let mut fuel = 0;
    for mass in BufReader::new(File::open("input")?).lines() {
        fuel += mass?.parse::<usize>().unwrap() / 3 - 2
    }
    Ok({println!("Fuel Needed: {}", fuel);})
}

#[cfg(feature="part2")]
fn main() -> std::io::Result<()> {
    let mut fuel = 0;
    for mass in BufReader::new(File::open("input")?).lines() {
        let mut mass = mass?.parse::<usize>().unwrap() / 3 - 2;
        while {fuel += mass; mass / 3 > 2} { mass = mass / 3 - 2 }
    }
    Ok({println!("Fuel Needed: {}", fuel);})
}


// Tests against the provided examples
#[test]
fn check() {
    fn fuel(mass: usize) -> usize {
        let mut total = 0;
        let mut mass = mass / 3 - 2;
        while {total += mass; mass / 3 > 2} { mass = mass / 3 - 2 };
        total
    }

    assert_eq!(fuel(12), 2);
    assert_eq!(fuel(1969), 966);
    assert_eq!(fuel(100756), 50346);
    assert_eq!(fuel(100756) + fuel(1969), 50346 + 966);
}
