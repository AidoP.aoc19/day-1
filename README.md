# Advent of Code Day 1

Solutions for part 1 and 2 of AoC 2019.

Part 2 is compiled by default.
`cargo run --no-default-features` for part 1